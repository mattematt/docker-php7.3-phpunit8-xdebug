# README #

This repo is a skeleton to develop PHP CLI applications and unit test them with PHPUnit

### What is this repository for? ###

* This repo is a skeleton to develop PHP 7.3 (http://php.net/) **CLI** applications and unit test them with PHPUnit (https://phpunit.de/getting-started/phpunit-8.html). This does **not** use composer. The idea is once the docker container is created you mount the ./app folder into the container and from there you can run the php scipts using the PHP CLI, and run tests in the same folder.
* Version 1.0

### How do I get set up? ###

git clone this repository and move into the directory on your command line. You must have docker installed (https://www.docker.com/get-started). In the directory run:

    docker build -t <image-name>:<version-number> .

The image name and version number can be whatever you like. Once the image is created you can start it. To see your list of images:

	docker images
And the image ID column is what you need to start the container. To run the container:

	docker run -it -v $(pwd)/app:/usr/app <image-id>
	
-it lets you interact with the shell and -v links the app folder in the directory to the directory in the container.

	cd /usr/app
To get to the directory inside the container. From there the php cli and the phpunit cli can be ran using:
	
	php test.php
	phpunit
respectively.

A skeleton php and test file is supplied in the /app folder.

### Notes ###
* Uses php-7.3.3
* Uses phpunit-8
* Uses XDebug 2.7
* Uses a UK mirror to download php