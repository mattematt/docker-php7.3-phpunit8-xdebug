FROM ubuntu

LABEL maintainer="Mattematt"

# Install PHP version 7.3.3

# Update the unbuntu repository
# wget to download php, build-essential to make and compile the lib, libxml2-dev needed to build php
# move to the folder to download
# download and build openssl with default settings, needed to build php with ssl
#download the file
# uncompress the file
# move into the previously uncompressed directory
# use the configure file to configure the install
# compile and build php with as many cores as is avaliable
# install the files
# make a directory where the developed files will be mounted
# Create an empty php.ini and tell php execulatable to look at it

RUN apt-get update && \
    apt-get install wget build-essential libxml2-dev libssl-dev -y &&\
    cd /usr/src/ &&\
    wget -O /usr/src/openssl-1.0.2o.tar.gz  https://www.openssl.org/source/openssl-1.0.2o.tar.gz && \
    tar -xzvf openssl-1.0.2o.tar.gz && \
    cd openssl-1.0.2o && \
    ./config && \
    make -j$(nproc) && \
    make install && \
    cd /usr/src/ &&\
    wget -O /usr/src/php.tar.bz2  http://uk1.php.net/get/php-7.3.3.tar.bz2/from/this/mirror && \
    tar -jxf php.tar.bz2 && \
    cd php-7.3.3/ && \
    ./configure --enable-maintainer-zts --enable-cli --with-openssl --with-zlib && \
    make -j$(nproc) && \
    make install && \
    mkdir /usr/app/ && \
    echo "" > /usr/local/lib/php.ini && \
    php -c /usr/local/lib/php.ini

# Install php unit

# Download the php archieve source, make it executable and put it in the path

RUN cd /usr/src && \
    wget -O phpunit https://phar.phpunit.de/phpunit-8.phar && \
    chmod +x phpunit && \
    cp phpunit /bin/

# Install XDebug

# Autoconf is used to use phpize which is used to build xdebug
# Install xdebug using PECL and then write the info to the config file
RUN apt-get install autoconf -y && \
    pecl install xdebug && \
    echo zend_extension=/usr/local/lib/php/extensions/no-debug-zts-20180731/xdebug.so >> /usr/local/lib/php.ini